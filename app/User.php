<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Collection;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    public function city()
    {
        return $this->belongsTo('App\City', 'city_id', 'id');
    }

    public function authorizeRoles($roles)
    {
        if (is_array($roles)) {
            return $this->hasAnyRole($roles) || false;
        }
        return $this->hasRole($roles) || false;
    }

    public function hasAnyRole($roles)
    {
        return null !== $this->roles()->whereIn('name', $roles)->first();
    }

    public function hasRole($role)
    {
        return null !== $this->roles()->where('name', $role)->first();
    }

    public function store($request)
    {
        $data = $request->all();

        if (empty($data['password'])) return false;

        $data['password'] = bcrypt($request->password);

        $user = self::create($data);

        if (!empty($data['role'])) {
            $roles = [];
            foreach ($data['role'] as $role)
                array_push($roles, $role);
            $user->roles()->sync($roles);
        }

        return $user;
    }

    public function edit($request, $id)
    {
        $user = self::findOrFail($id);
        $request_user = $request->all();

        if ($user->email == $request->email) {
            $request_user = $request->except('email');
        }

        return $user->fill($request_user)->save();
    }
}
