<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**f
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        if ($request->user()->authorizeRoles(config('global.boss'))) {
            return $next($request);
        }

        return redirect()->route('login');
    }
}
