<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Validator;
use App\Http\Requests\LoginRequest;

class LoginController extends Controller
{
    public function getLogin()
    {
        return view('login');
    }

    public function postLogin(LoginRequest $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');
        $role = true;

        if (Auth::attempt(['email' => $email, 'password' => $password], $request->has('remember'))) {
            if (Auth::user()->authorizeRoles(config('global.boss'))) {
                $role = false;
            }
            if ($role) {
                Auth::logout();
                $message = ['Tài khoản không có quyền truy nhập'];
            } else {
                $message = ['Đăng nhập thành công'];
            }
        } else {
            $role = true;
            $message = ['Email hoặc mật khẩu không đúng'];
        }
        return response()->json([
            'error' => $role,
            'message' => $message,
        ], 200);
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->back();
    }

}
