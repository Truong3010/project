<?php

namespace App\Http\Controllers;

use App\City;
use App\Http\Requests\CityRequest;
use Illuminate\Http\Request;

class CityController extends Controller
{

    public function index()
    {
        $cities = City::latest()->paginate(6);

        return view('admin.city.index-city', compact('cities'));
    }

    public function create()
    {
        //
    }

    public function store(CityRequest $request)
    {
        $city = City::create($request->all());

        return response()->json([
            'city' => $city,
            'message' => 'Thêm thành công',
        ]);
    }

    public function show($id)
    {
        $city = City::findOrFail($id);

        return response()->json([
            'city' => $city,
        ]);
    }

    public function edit(City $city)
    {
        //
    }

    public function update(CityRequest $request, $id)
    {
        $city = City::findOrFail($id);

        $city->fill($request->all())->save();

        return response()->json([
            'city' => $city,
            'message' => 'Sửa thành công',
        ]);
    }

    public function destroy($id)
    {
        City::findOrFail($id)->delete();

        return response()->json([
            'message' => 'Xóa thành công',
        ]);
    }
}
