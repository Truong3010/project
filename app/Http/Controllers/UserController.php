<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\User;
use App\Http\Requests\UserRequest;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index(Request $request)
    {
        $users = User::with('roles')->latest()->get();
        $roles = Role::all();

        if (!empty($request)) {
            if ($request->ajax()) {
                return Datatables::of($users)
                    ->addIndexColumn()
                    ->addColumn('role', function ($row) {
                        $view = "";
                        foreach ($row->roles as $role)
                            $view = $view . '<p>' . $role->name . '</p>';
                        return $view;
                    })
                    ->addColumn('last', function ($row) {
                        return \Carbon\Carbon::parse($row->updated_at)->diffForHumans();
                    })
                    ->addColumn('action', function ($row) {

                        $btn = '<button class="edit-modal btn btn-info" data-id="' . $row->id . '">'
                            . '<span class="glyphicon glyphicon-edit"></span> Edit'
                            . '</button>';

                        $btn = $btn . '<button class="delete-modal btn btn-danger" data-id="' . $row->id . '" data-name="' . $row->name . '">'
                            . '<span class="glyphicon glyphicon-trash"></span> Delete'
                            . '</button>';

                        return $btn;
                    })
                    ->rawColumns(['action', 'role'])
                    ->make(true);
            }
        }

        return view('admin.user.user-index', compact('users', 'roles'));
    }

    public function store(UserRequest $request)
    {
        $user = $this->user->store($request);

        if ($user) {
            return response()->json([
                'message' => 'Thêm thành công',
            ]);
        } else {
            return response()->json([
                'error' => 'Thiếu mật khẩu',
            ]);
        }

    }

    public function show($id)
    {
        $user = User::findOrFail($id);

        return response()->json($user);
    }

    public function update(Request $request, $id)
    {
        $this->user->edit($request, $id);

        return response()->json([
            'message' => 'Chỉnh sửa thành công',
        ]);
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return response()->json([
            'message' => 'Đã xóa',
        ]);
    }
}
