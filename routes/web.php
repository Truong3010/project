<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'admin', 'middleware' => ['role', 'auth']], function () {
    Route::resource('users', 'UserController')->except(['create', 'edit']);
    Route::resource('cities', 'CityController')->only(['index']);
//    Route::resource('cities', 'CityController');
});

Route::get('admin/login', 'LoginController@getLogin');
Route::post('admin/login', 'LoginController@postLogin')->name('login');
Route::get('admin/logout', 'LoginController@logout');

