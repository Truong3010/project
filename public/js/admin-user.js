$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var table = $('#dataTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: $("#dataTable").data('url'),
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'role', name: 'role'},
            {data: 'last', name: 'last'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    // add a new user
    $(document).on('click', '.add-modal', function () {
        $('.modal-title').text('Add');
        $('#createtForm').trigger("reset");
        $('#addModal').modal('show');
    });

    $('.modal-footer').on('click', '.add', function (e) {
        e.preventDefault();

        $.ajax({
            data: $('#createtForm').serialize(),
            url: 'users',
            type: "POST",
            success: saveUser,
            error: getError,
        });
    });

    // Edit a user
    $(document).on('click', '.edit-modal', function () {
        id = $(this).data('id');
        $.get('users/' + id, function (data) {
            $('.modal-title').text('Edit');
            $('#id_edit').val(data.id);
            $('#name_edit').val(data.name);
            $('#email_edit').val(data.email);
            $('#editModal').modal('show');
        })
    });


    $('.modal-footer').on('click', '.edit', function (e) {
        e.preventDefault();

        $.ajax({
            data: $('#updateForm').serialize(),
            url: 'users/' + id,
            type: "PATCH",
            dataType: 'json',
            success: saveUser,
            error: getError,
        });
    });

    // delete a user
    $(document).on('click', '.delete-modal', function () {
        $('.modal-title').text('Delete');
        $('#deleteModal').modal('show');
        id = $(this).data('id');
    });

    $('.modal-footer').on('click', '.delete', function (e) {
        $.ajax({
            type: "DELETE",
            url: 'users/' + id,
            success: deleteUser,
            error: getError,
        });
    });

    function saveUser(data) {
        if (data.error) {
            toastr.error(data.error);
        } else {
            toastr.success(data.message);
            $('#ajaxModel').modal('hide');
            table.draw();
        }
    }

    function getError(data) {
        res = $.parseJSON(data.responseText);
        $.each(res.errors, function (index, value) {
            toastr.error(value);
        });
    }

    function deleteUser(data) {
        toastr.warning(data.message);
        $('#ajaxModel').modal('hide');
        table.draw();
    }

});
