$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // add a new user
    $('#wrapper').on('click', '.add-modal', function () {
        $('.modal-title').text('Add');
        $('#createtForm').trigger("reset");
        $('#addModal').modal('show');
    });

    $('.modal-footer').on('click', '.add', function (e) {
        e.preventDefault();

        $.ajax({
            data: $('#createtForm').serialize(),
            url: window.location.origin + '/api/city',
            type: "POST",
            success: createUser,
            error: getError,
        });
    });

    // Edit a user
    $('#wrapper').on('click', '.edit-modal', function () {
        id = $(this).data('id');
        $.get(window.location.origin + '/api/city/' + id, function (data) {
            city = data.city;
            $('.modal-title').text('Edit');
            $('#id_edit').val(city.id);
            $('#name_edit').val(city.name);
            $('#editModal').modal('show');
        })
    });


    $('.modal-footer').on('click', '.edit', function (e) {
        e.preventDefault();

        $.ajax({
            data: $('#updateForm').serialize(),
            url: window.location.origin + '/api/city/' + id,
            type: "PATCH",
            success: saveUser,
            error: getError,
        });
    });

    // delete a user
    $('#wrapper').on('click', '.delete-modal', function () {
        $('.modal-title').text('Delete');
        $('#deleteModal').modal('show');
        id = $(this).data('id');
    });

    $('.modal-footer').on('click', '.delete', function (e) {
        $.ajax({
            type: "DELETE",
            url: window.location.origin + '/api/city/' + id,
            success: deleteUser,
            error: getError,
        });
    });

    function createUser(data) {
        if (data.error) {
            toastr.error(data.error);
        } else {
            let city = data.city;
            toastr.success(data.message);
            $('#cityDataTable').append(getHtml(city));

            $('#ajaxModel').modal('hide');
        }
    }

    function saveUser(data) {
        if (data.error) {
            toastr.error(data.error);
        } else {
            let city = data.city;
            toastr.success(data.message);
            $(".city_id_" + id).replaceWith(getHtml(city));

            $('#ajaxModel').modal('hide');
        }
    }

    function getError(data) {
        res = $.parseJSON(data.responseText);
        $.each(res.errors, function (index, value) {
            toastr.error(value);
        });
    }

    function deleteUser(data) {
        toastr.warning(data.message);
        $(".city_id_" + id).remove();
        $('#ajaxModel').modal('hide');
    }

    function getHtml(city) {
        var html = '<tr class="city_id_' + city.id + '">';
        html += '<td>' + city.id + '</td>';
        html += '<td>' + city.name + '</td>';
        html += '<td> 0 </td>';
        html += '<td> 1 second ago </td>';
        html += '<td>';
        html += '   <button class="edit-modal btn btn-info m-1" data-id="' + city.id + '">Update</button>';
        html += '   <button class="delete-modal btn btn-danger" data-id="' + city.id + '">Delete</button>';
        html += '</td>';
        html += '</tr>';
        return html;
    }

});
