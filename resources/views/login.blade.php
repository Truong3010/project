<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin - Login</title>

    <!-- Custom fonts for this template-->
    <link href="{{ asset('admin/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template-->
    <link href="{{ asset('admin/css/sb-admin.css') }}" rel="stylesheet">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">


</head>

<body class="bg-dark">

<div class="container">
    <div class="card card-login mx-auto mt-5">
        <div class="card-header">Login</div>
        <div class="card-body">
            <form id="loginFrom" action="#" data-url="{{ route("login") }}" data-admin="{{ route('users.index') }}" method="POST" role="form">
                {{ csrf_field() }}
                <div class="alert alert-danger error errorLogin" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p style="color:red; display:none;" class="error errorLogin"></p>
                </div>
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="text" class="form-control" id="email" placeholder="Email" name="email" value="{{old('email')}}">
                    <p style="color:red; display: none" class="error errorEmail"></p>
                </div>
                <div class="form-group">
                    <label for="">Password</label>
                    <input type="password" class="form-control" id="password" placeholder="Password" name="password">
                    <p style="color:red; display: none" class="error errorPassword"></p>
                </div>
                <div class="form-group form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox"> Ghi nhớ
                    </label>
                </div>
                <button id="dang-nhap" type="submit" class="btn btn-primary btn-block">Đăng nhập</button>
            </form>
            <div class="text-center">
                <a class="d-block small mt-3" href="register.html">Register an Account</a>
                <a class="d-block small" href="forgot-password.html">Forgot Password?</a>
            </div>
            <div class="alert alert-danger print-error-msg" style="display:none">
                <ul></ul>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="{{ asset('admin/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('admin/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- Core plugin JavaScript-->
<script src="{{ asset('admin/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script>
    $(function() {
        $('#dang-nhap').click(function (e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: $('#loginFrom').attr('data-url'),
                data: {
                    'email': $('#email').val(),
                    'password': $('#password').val()
                },
                type: "POST",
                dataType: "json",
                success: function (data) {
                    if (data.error) {
                        $('.error').hide();
                        if (data.message != undefined) {
                            $.each(data.message, function (index, value) {
                                toastr.warning(value);
                            });
                        }
                    } else {
                        $('.error').hide();
                        toastr.success(data.message);
                        location.assign( $('#loginFrom').attr('data-admin') );
                    }
                },
                error: function(data) {
                    res = $.parseJSON(data.responseText);
                    $.each(res.errors, function (index, value) {
                        toastr.warning(value);
                    });
                }
            });
        })
    });
</script>

</body>

</html>
