@extends('layouts.admin')

@section('title', 'Page Title')

@section('content')

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Admin</a>
            </li>
            <li class="breadcrumb-item active">User</li>
        </ol>

        <!-- DataTables Example -->
        <div class="card mb-3">
            <div class="card-header">
                <i class="fas fa-table"></i>
                Data Table User
            </div>
            <a href="#" class="add-modal btn btn-success">Thêm user</a>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="postTable table table-bordered data-table" id="dataTable" width="100%"
                           cellspacing="0" data-url="{{ route('users.index') }}">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Last updated</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Last updated</th>
                            <th>Role</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                        <tbody id="tbody">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->

    @include('admin.user.modal')
@endsection

@section('script')
    <script src="{{ asset('js/admin-user.js') }}"></script>
@endsection
