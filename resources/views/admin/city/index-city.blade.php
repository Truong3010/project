@extends('layouts.admin')

@section('title', 'Page Title')

@section('content')

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Admin</a>
            </li>
            <li class="breadcrumb-item active">City</li>
        </ol>

        <!-- DataTables Example -->
        <div class="card mb-3">
            <div class="card-header">
                <i class="fas fa-table"></i>
                City
            </div>
            <div class="card-body">
                <a href="#" class="add-modal btn btn-success">Thêm thành phố mới</a>
                <table id="cityDataTable" class="table table-striped">
                    <thead class="thead-dark">
                    <tr>
                        <td>ID</td>
                        <td>Name</td>
                        <td>Users</td>
                        <td>Updated at</td>
                        <td>Action</td>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <td>ID</td>
                        <td>Name</td>
                        <td>Users</td>
                        <td>Updated at</td>
                        <td>Action</td>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($cities as $city)
                        <tr class="city_id_{{ $city->id }}">
                            <td>{{ $city->id }}</td>
                            <td>{{ $city->name }}</td>
                            <td>{{ count($city->users) }}</td>
                            <td>
                                {{ \Carbon\Carbon::parse($city->updated_at)->diffForHumans() }}
                            </td>
                            <td>
                                <button class="edit-modal btn btn-info m-1" data-id="{{ $city->id }}">Update</button>
                                <button class="delete-modal btn btn-danger" data-id="{{ $city->id }}">Delete</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $cities->links() !!}
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->

    @include('admin.city.modal')
@endsection

@section('script')
    <script src="{{ asset('js/admin/city.js') }}"></script>
@endsection
